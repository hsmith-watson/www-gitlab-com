<!--
Contributor Success uses this issue to collect MVPs for the release post. This issue is created by [release post branch creation task](https://about.gitlab.com/handbook/marketing/blog/release-posts/#release-post-branch-creation-rake-task)
-->

# Overview

**MVP Nominations for [_MILESTONE_ release post](RP_MR_LINK)**

1. The purpose of this issue is to collect MVP nominations for this release from the product team, community relations, and MR coaches.
1. The Contributor Success team will share a link to this Issue in the `#release-post`, `#community-relations`, `#mr-coaching` and `#core` Slack channels to encourage GitLab team members to share MVP nominations.
1. On the 12th of the month, Contributor Success will encourage GitLab team members to vote on the nominations.
1. On the 15th of the month, Contributor Success will choose the nominee with the most votes.

## Nominate a contributor

@gitlab-org/community-relations/contributor-success @gl-product @gitlab-org/coaches @gitlab-com/marketing/community-relations Please review the [Community Contributions for MVP consideration](https://app.periscopedata.com/app/gitlab/1058191/Community-Contributions-for-MVP-consideration) and nominate a contributor in this issue.

1. Review the [Community Contributions for MVP consideration](https://app.periscopedata.com/app/gitlab/1058191/Community-Contributions-for-MVP-consideration)
1. Add a comment to this issue with the contributor name, GitLab profile, and some details of their contribution.

## To vote for a nominee

- Add 👍 to place a vote for a nominee

/assign @daniel-murphy
/label ~"Contributor Success"
/label ~"contributorgrowth::increase value"
/milestone %_MILESTONE_
