---
layout: handbook-page-toc
title: "Annual Field Security Study"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc .hidden-md .hidden-lg}

## Overview
GitLab conducts an annual Field Security Study to document areas of concern or improvement identified with GitLab's enterprise security, market competitiveness, and ability to support its customers' needs. By assessing the perceived impact rating and providing clear and centralized insight on customer feedback to GitLab's leadership, we can drive cross-organizational alignment on a strategy to iterate on GitLab's product and security programs.

Reports are considered [Internal](https://about.gitlab.com/handbook/communication/confidentiality-levels/#internal)

### Frequency
Annually during Q4

### Leadership Sponsor
Julia Lake, Sr. Director, Security Assurance

### DRI
The Field Security team is responsible for collecting and analyzing the data, crafting and validating potential solutions, and tracking future remediation activities. 

## Process

### Data Collection

The data for this report is collected from various sources, including:

- Customer feedback delivered to Field Security as part of security questionnaires, customer calls, and other customer assurance activities 
- Customer feedback delivered to internal departments such as Sales, Support, Security, Legal, Infrastructure, and Customer Success
- Competitor product comparisons to GitLab using publicly available data sources
- Third-Party Cyber Security and Risk reporting services such as BitSight and Qualys SSL
- Industry perception of GitLab security via media publishings (articles, blogs, & public forums) 
- Concerns and results from the prior year's report

**The quarterly Field Security research reports will be a primary input when developing the annual Field Security Study.**

### Data Analysis

The following analysis will occur based on the collected data:

- Evaluation of concerns from the prior year to include remediation status and updated considerations.  
- Evaluation of current areas of customer concern.
- Competitive analysis to include a comparison of available security features, security features maturation and security compliance offerings.
- Documentation of recommendations based on field feedback. Note, these should not be considered the final plan for remediation as that will be the responsibility of the GitLab DRI to define. 

### Reporting and Remediation

A detailed report is produced and provided to relevant [internal](https://about.gitlab.com/handbook/communication/confidentiality-levels/#internal) stakeholders for consideration. Prioritized concerns will also be documented in GitLab issues by the Field Security team and assigned to the appropriate team members. 


- Information Security related issues will be evaluated and managed by Security Leadership.

## Listing of Reports (Internal Only) 
- [FY21 GitLab.com Field Security Study](https://docs.google.com/document/d/1BN979fDYeOwIhKOvrMStyqj5ftDq1w0bc2urcsk3z8U/edit?usp=sharing) report and [Epic](https://gitlab.com/groups/gitlab-com/-/epics/957)
- A Field Security Study report was not delivered for FY22.
- [FY23 Field Security Study](https://docs.google.com/document/d/1QD55PuhfO15hWFaz7BoLiHLxMU7q3JuFJ5EbB57MH9s/edit) report.

<div class="flex-row" markdown="0" style="height:40px">
    <a href="https://about.gitlab.com/handbook/security/security-assurance/field-security/" class="btn btn-purple-inv" style="width:100%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Return to the Field Security Homepage</a>
</div> 
